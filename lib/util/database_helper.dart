import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:notodo/model/NoToDo_item.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class database_helper {
  static final database_helper _instacne = new database_helper.internal();
  final String tableuser="notodo";
  final String column_id="id";
  final String column_itemname="itemname";
  final String column_datecreted="datecreted";


  factory database_helper() => _instacne;

  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initdb();
    return _db;
  }

  database_helper.internal();

  initdb() async {
    Directory dir = await getApplicationDocumentsDirectory();
    String path = join(dir.path, "notodo.db");
    var ourdb = await openDatabase(path, version: 1, onCreate: _Oncreat);
    return ourdb;
  }
/*
* CREATE TABLE LIKE THIS
*
*     ID(PRIMARY KEY)  |   USERNAME   | PASSWORD
*    ---------------------------------------------
*         1                  ABD            OLABI
*         2                  BODI           OLAB
*
*
* */

//CRUD: create read update delete
  void _Oncreat(Database db, int version) async {
    await db.execute(
        "CREATE TABLE $tableuser($column_id INTEGER PRIMARY KEY,$column_itemname  TEXT,$column_datecreted TEXT)");




  }
  Future<List>getAllItems()async{
    var dbclient =await db;
    var result=await dbclient.rawQuery("SELECT * FROM $tableuser  ORDER BY $column_id ASC ");
    return result.toList();

  }

  Future<int> SaveItem(notodo_item item )async
  {
    var   dbclient=await db;
    int result=await dbclient.insert(tableuser, item.toMap());
    return result;



  }




  Future<int> getCount ()async{
    var dbclient=await db;
    return Sqflite.firstIntValue(
        await dbclient.rawQuery("SELECT COUNT(*) FROM $tableuser")

    );

  }


  Future<notodo_item> getItem(int id )async{

    var dbclient = await db;
    var result =await dbclient.rawQuery("SELECT * FROM $tableuser WHERE $column_id=$id  ");
    if(result.length==0)
      return null;
    else
      return new notodo_item.frommap(result.first);
  }

  Future<int> deleteitem(int id)async{
    var dbcilent=await db;
    return dbcilent.delete(tableuser,where: "$column_id= ?",whereArgs: [id]);


  }


  Future<int> updateItem(notodo_item Item)async{
    var dbclient=await db;
    return await dbclient.update(tableuser,Item.toMap(),where:"$column_id = ?",whereArgs: [Item.id]);


  }
  Future close()async{
    var dbclient =await db;
    return dbclient.close();

  }

}
