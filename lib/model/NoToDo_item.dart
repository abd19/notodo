import 'package:flutter/material.dart';

class notodo_item extends StatelessWidget {
  String _itemname;
  String _datacreted;
  int _id;

  notodo_item(this._itemname, this._datacreted);

  notodo_item.map(dynamic obj) {
    this._itemname = obj["itemname"];
    this._datacreted = obj["datecreted"];
    this._id = obj["id"];
  }

  String get itemname => _itemname;

  String get datacreted => _datacreted;

  int get id => _id;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["itemname"] = _itemname;
    map["datecreted"] = _datacreted;
    if (_id != null) {
      map["id"] = _id;
    }
    return map;
  }

  notodo_item.frommap(Map<String, dynamic> map) {
    this._itemname = map["itemname"];
    this._datacreted = map["datecreted"];
    this._id = map["id"];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(8.5),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Column(
crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                _itemname,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top:4.5),
                child: Text("Created on: $_datacreted",
                  style: TextStyle(
                      color: Colors.white70,
                      fontSize: 13.2,
                      fontStyle: FontStyle.italic

                  ),),
              ),


            ],
          )


        ],
      ),
    );
  }
}
