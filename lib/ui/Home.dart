import 'package:flutter/material.dart';
import 'package:notodo/ui/NoToDoscreen.dart';


class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("NoToDo"),
        backgroundColor: Colors.black12,
        centerTitle: true,
      ),
body: new NoToDoscreen(),
    );
  }
}
