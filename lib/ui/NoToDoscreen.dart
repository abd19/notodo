import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:notodo/model/NoToDo_item.dart';
import 'package:notodo/model/NoToDo_item.dart' as prefix0;
import 'package:notodo/util/database_helper.dart';

class NoToDoscreen extends StatefulWidget {
  @override
  _NoToDoscreenState createState() => _NoToDoscreenState();
}

class _NoToDoscreenState extends State<NoToDoscreen> {
  final _textediting = new TextEditingController();
  var db = new database_helper();
  final List<notodo_item> _itemlist = <notodo_item>[];

  @override
  void initState() {
    super.initState();
    _readnodolist();
  }

  Future<int> _handelSubmit(String text) async {
    _textediting.clear();
    notodo_item noToDo =
        new notodo_item(text, DateTime.now().toIso8601String());
    int Saveditemid = await db.SaveItem(noToDo);
    notodo_item addedItem = await db.getItem(Saveditemid);
    setState(() {
      _itemlist.add(addedItem);
    });

    return Saveditemid;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      body: new Column(
        children: <Widget>[
          new Flexible(
              child: ListView.builder(
                  padding: new EdgeInsets.all(8.0),
                  reverse: false,
                  itemCount: _itemlist.length,

                  itemBuilder: (_, int i) {
                    return new Card(

                      color: Colors.white10,
                      child: new ListTile(
                        title: _itemlist[i],
                        onLongPress: ()=>_EditInfo(_itemlist[i], i),
                        trailing: new Listener(
                          key: new Key(_itemlist[i].itemname),
                          child: new Icon(
                            Icons.remove_circle,
                            color: Colors.redAccent,
                          ),
                          onPointerDown: (pointerEvent) {
                            _deletenodo(_itemlist[i].id, i);
                          },
                        ),
                      ),
                    );
                  })),
          new Divider(
            height: 2,
          )
        ],
      ),
      floatingActionButton: new FloatingActionButton(
          tooltip: "Add Item",
          backgroundColor: Colors.redAccent,
          child: new ListTile(
            title: new Icon(Icons.add),
          ),
          onPressed: _showForm),
    );
  }

  _showForm() {
    var alert = new AlertDialog(
      title: Text("Add Item"),
      content: Row(
        children: <Widget>[
          Expanded(
            child: TextField(
              controller: _textediting,
              autofocus: true,
              decoration: new InputDecoration(
                  labelText: "Item",
                  hintText: "eg. don't buy stuff",
                  icon: Icon(Icons.note_add)),
            ),
          )
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            var result = _handelSubmit(_textediting.text);
            if (result != 0) {
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text(
                  "Item added ",
                  textAlign: TextAlign.center,
                ),
              ));
            }
            Navigator.of(context).pop();
          },
          child: Text("Save"),
        ),
        new FlatButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text("Cancel"),
        )
      ],
    );

    showDialog(context: context, child: alert);
  }

  _readnodolist() async {
    List items = await db.getAllItems();
    items.forEach((item) {
      notodo_item nodoitem = notodo_item.map(item);
      setState(() {
        _itemlist.add(notodo_item.map(item));
      });
      print("DB items :${nodoitem.itemname}");
    });
  }

  _deletenodo(int id, int index) async {
    debugPrint("deleting item!");
    if (1 == await db.deleteitem(id)) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          "Item deleted ",
          textAlign: TextAlign.center,
        ),
      ));
    }

    setState(() {
      _itemlist.removeAt(index);
    });
  }

  _EditInfo(notodo_item item, int i) {
    var alert = new AlertDialog(
      title: Text("Edit Item "),
      content: new Row(
        children: <Widget>[
          new Expanded(
              child: new TextField(
            controller: _textediting,
            autofocus: true,
            decoration: new InputDecoration(
                labelText: "Item",
                hintText: "e.g eat too much",
                icon: Icon(Icons.update)),
          ))
        ],
      ),
      actions: <Widget>[
        new FlatButton(
            onPressed: () async {
              notodo_item updatedone = new notodo_item.frommap({
                "itemname": _textediting.text,
                "datecreted": DateTime.now().toString(),
                "id": item.id,
              });
              _handelUpdate(i, item);
              await db.updateItem(updatedone);
              setState(() {
                _readnodolist();
              });
              Navigator.of(context).pop();
            },
            child: new Text("Update")),
        new FlatButton(
            onPressed: () =>
              Navigator.of(context).pop() ,
            child: Text("Cancel"))
      ],
    );
   showDialog(context: context,child: alert);
  }

  void _handelUpdate(int i, notodo_item item) async {
    setState(() {
      _itemlist.removeWhere((e) {
        _itemlist[i].itemname == item.itemname;
      });
    });
  }
}
